# cs586-dbintro-project

### Bradon Kanyid - Summer 2021

## Description

For my project, I took my last.fm listening history, which is a record of *most* of the songs I listened to digitally since 2006, and joined it to artist/track/genre data that I generated via the Spotify API, in order to find interesting aspects about my own listening history that could not be found from either on their own, especially since I've only been using Spotify for the last few years.

## Dataset Details

### Initial Data

There was a bug in the last.fm exporter that included the song I was listening to at the time of the export into every 200-song dump it did, with no timestamp. I'm filtering that out of the dataset during data engineering.

```bash
> cat listening_history.csv| grep -v -E "Panopticon,...and again into the light,The Embers at Dawn,$" | wc -l

127828
```

### After Data Engineering

From the initial 127828 listens that I had recorded, I ended up dumping any that Spotify could not find in its API. That was a loss of about 30,000 listens, unfortunately. Perhaps with fancier heuristics I could have found more matches from Spotify, but I'm also not surprised that a decent subset of my listening history is unavailable on Spotify, as I have local artists and such that have never existed on Spotify.

```sql
SELECT count(*) FROM scrobbles; 
```

```
 count  
--------
 107563
(1 row)
```

```sql
SELECT * FROM scrobbles LIMIT 1;
```

```
       timestamp        |       spotify_id
------------------------+------------------------
 2021-07-07 04:40:00-07 | 4p4qSBvMGa7BQrtSKdLNGq
(1 row)
```

---

```sql
SELECT count(*) FROM spotify_tracks;
```

```
 count 
-------
 30849
(1 row)
```

```sql
SELECT * FROM spotify_tracks LIMIT 1;
```

```
       spotify_id       |       artist_id        |            album            |       track        | popularity | time_signature |  tempo  | liveness | instrumentalness | duration_ms | energy | acousticness
------------------------+------------------------+-----------------------------+--------------------+------------+----------------+---------+----------+------------------+-------------+--------+--------------
 3dGmmX5F9aeoiwzCbXUoph | 2Mz5qpR3WxbcBwZBsmraWE | ...And Again Into the Light | The Embers at Dawn |         29 |              3 | 120.114 |    0.543 |            0.952 |      760328 |  0.416 |      0.00472
(1 row)
```

---

```sql
SELECT count(*) FROM artist;
```

```
 count 
-------
  3433
(1 row)
```

```sql
SELECT * FROM artist LIMIT 1;
```

```
       spotify_id       |     name
------------------------+--------------
 3sjqdc4V86Y8fAzdmYZoH2 | Nocturnus AD
(1 row)
```

---

```sql
SELECT count(*) FROM artist_genre_rel;
```

```
 count
-------
 13191
(1 row)
```

```sql
SELECT * FROM artist_genre_rel LIMIT 1;
```

```
       artist_id        | genre_id
------------------------+----------
 3sjqdc4V86Y8fAzdmYZoH2 |        1
(1 row)
```

---

```sql
SELECT count(*) FROM genre;
```

```
 count 
-------
  1543
(1 row)
```

```sql
SELECT * FROM genre LIMIT 1;
```

```
 genre_id |        name
----------+---------------------
        1 | florida death metal
(1 row)
```

## Questions, Queries, and Results

### 1) Which artists were my top 10 artists in 2010?

```sql
SELECT a.name , COUNT(*) AS track_count_by_artist
FROM scrobbles s
JOIN spotify_tracks st ON s.spotify_id=st.spotify_id
JOIN artist a ON st.artist_id=a.spotify_id
WHERE EXTRACT(YEAR from s.timestamp) = 2010
GROUP BY a.name
ORDER BY count(*) desc
LIMIT 10;
```

```
      name       | track_count_by_artist
-----------------+-----------------------
 Dangers         |                   198
 Outbreak        |                   154
 Vampire Weekend |                   116
 Justice         |                    74
 Paint It Black  |                    60
 Minutemen       |                    50
 Jay Reatard     |                    46
 Daughters       |                    41
 Defeater        |                    39
 Modest Mouse    |                    36
(10 rows)
```

---

### 2) What specific tracks did I listen to most in 2015?

```sql
SELECT DISTINCT a1.name, st1.album, st1.track, m.track_count
FROM scrobbles s1
JOIN spotify_tracks st1   ON s1.spotify_id=st1.spotify_id
JOIN artist a1            ON st1.artist_id=a1.spotify_id
JOIN (
  SELECT s.spotify_id, COUNT(*) AS track_count
  FROM scrobbles s
  WHERE EXTRACT(YEAR from s.timestamp) = 2015
  GROUP BY s.spotify_id
  ORDER BY COUNT(*) DESC
  LIMIT 10
) AS m ON m.spotify_id=s1.spotify_id
ORDER BY m.track_count DESC;
```

```
                name                |       album        |         track          | track_count
------------------------------------+--------------------+------------------------+-------------
 Beyond Creation                    | The Aura           | Omnipresent Perception |          23
 Drewsif Stalin's Musical Endeavors | ...Comes to an End | Collapse               |          23
 Drewsif Stalin's Musical Endeavors | ...Comes to an End | Nightfall              |          20
 Drewsif Stalin's Musical Endeavors | ...Comes to an End | Seldom                 |          20
 Rivers of Nihil                    | Monarchy           | Sand Baptism           |          20
 Drewsif Stalin's Musical Endeavors | ...Comes to an End | Desolate               |          19
 Drewsif Stalin's Musical Endeavors | ...Comes to an End | Sisyphus               |          19
 Drewsif Stalin's Musical Endeavors | ...Comes to an End | Ultimatum              |          19
 Fallujah                           | The Harvest Wombs  | Cerebral Hybridization |          19
 Fallujah                           | The Flesh Prevails | Allure                 |          17
(10 rows)
```

---

### 3) What song(s) have I listened to the most times at exactly the same time on different days?

```sql
SELECT a.name, st.track, st.album, same_time.pst_time, same_time.same_time_count
FROM spotify_tracks st
JOIN artist a ON a.spotify_id=st.artist_id
JOIN (
  SELECT s1.spotify_id, s1.timestamp::time - '7 hours'::interval AS pst_time, count(*) AS same_time_count
  FROM scrobbles AS s1 JOIN scrobbles AS s2
  ON s1.spotify_id=s2.spotify_id
  WHERE s1.timestamp::time=s2.timestamp::time AND s1.timestamp>s2.timestamp
  GROUP BY s1.spotify_id, s1.timestamp::time
  ORDER BY count(*) DESC
) AS same_time ON st.spotify_id=same_time.spotify_id
WHERE same_time.same_time_count=(
  SELECT max(m) FROM (
    SELECT count(*) AS m
    FROM scrobbles AS s1 JOIN scrobbles AS s2
    ON s1.spotify_id=s2.spotify_id
    WHERE s1.timestamp::time=s2.timestamp::time AND s1.timestamp>s2.timestamp
    GROUP BY s1.spotify_id, s1.timestamp::time
    ORDER BY count(*) DESC
  ) AS n
);
```

```
 name  |    track    |      album      | pst_time | same_time_count 
-------+-------------+-----------------+----------+-----------------
 alt-J | Dissolve Me | An Awesome Wave | 19:07:00 |               3
 VOLA  | Starburn    | Inmazes         | 14:14:00 |               3
(2 rows)
```

#### Comment
Wow! I listened to these two songs at exactly the same hour and minute on 3 different occasions!

It's filtered because I only put MAX here, but there were more than 400 occasions where I had this happen on 2 occasions, for other songs!

---

### 4) What year was my most “busy” year (which year did I record the most listens to individual tracks?)

```sql
SELECT EXTRACT(YEAR from timestamp) AS year,count(*) as total_count
FROM scrobbles
GROUP BY year
HAVING count(*)=(
  SELECT MAX(m.total_count) FROM (
    SELECT EXTRACT(YEAR from timestamp) AS year,count(*) AS total_count 
    FROM scrobbles 
    GROUP BY year
  ) as m
);
```

```
 year | total_count
------+-------------
 2020 |       12780
(1 row)
```

---

### 5) What month was my least “busy” month (which month did I record the least listens year-over-year?)

```sql
SELECT TO_CHAR( TO_DATE (EXTRACT(MONTH from timestamp)::text, 'MM'), 'Month') AS month, COUNT(*) AS total_count
FROM scrobbles
GROUP BY month
HAVING COUNT(*)=(
  SELECT MIN(m.total_count) FROM (
    SELECT EXTRACT(MONTH from timestamp) AS month,COUNT(*) AS total_count 
    FROM scrobbles 
    GROUP BY month
  ) as m
);
```

```
   month   | total_count
-----------+-------------
 September |        7083
(1 row)
```

---

### 6) For my favorite artists, what are the albums I like least to most, based on # of listens?

```sql
WITH favorite_artists(id, track_count) AS (
  SELECT st.artist_id, COUNT(*) AS track_count_by_artist
  FROM scrobbles s
  JOIN spotify_tracks st ON s.spotify_id=st.spotify_id
  GROUP BY st.artist_id
  ORDER BY count(*) DESC
  LIMIT 10
)
    SELECT a.name, st.album, count(*)
    FROM scrobbles s 
    JOIN spotify_tracks st ON s.spotify_id=st.spotify_id
    JOIN favorite_artists fa ON st.artist_id=fa.id
    JOIN artist a ON a.spotify_id=st.artist_id
    GROUP BY a.name, st.album
    HAVING count(*)>10
    ORDER BY a.name, count(*);
```

```
               name               |                      album                      | count
----------------------------------+-------------------------------------------------+-------
 A Wilhelm Scream                 | A Wilhelm Scream                                |    47
 A Wilhelm Scream                 | Mute Print                                      |   101
 A Wilhelm Scream                 | Partycrasher                                    |   218
 A Wilhelm Scream                 | Ruiner                                          |   325
 A Wilhelm Scream                 | Career Suicide                                  |   328
 Between The Buried And Me        | Colors (2020 Remix / Remaster)                  |    13
 Between The Buried And Me        | The Great Misdirect (2019 Remix / Remaster)     |    26
 Between The Buried And Me        | Future Sequence: Live at the Fidelitorium       |    34
 Between The Buried And Me        | The Silent Circus                               |    37
 Between The Buried And Me        | Automata II                                     |    62
 Between The Buried And Me        | The Great Misdirect                             |    67
 Between The Buried And Me        | The Parallax: Hypersleep Dialogues              |    72
 Between The Buried And Me        | Alaska                                          |    90
 Between The Buried And Me        | Coma Ecliptic                                   |   160
 Between The Buried And Me        | Automata I                                      |   167
 Between The Buried And Me        | Colors                                          |   191
 Between The Buried And Me        | The Parallax II: Future Sequence                |   323
 Converge                         | When Forever Comes Crashing                     |    18
 Converge                         | Beautiful Ruin                                  |    19
 Converge                         | Petitioning The Empty Sky                       |    20
 Converge                         | You Fail Me Redux                               |    40
 Converge                         | No Heroes                                       |    42
 Converge                         | Jane Doe                                        |    70
 Converge                         | The Dusk In Us                                  |   229
 Converge                         | Axe To Fall                                     |   281
 Converge                         | All We Love We Leave Behind (Deluxe Edition)    |   361
 Gojira                           | Les Enfants Sauvages                            |    11
 Gojira                           | Fortitude                                       |    21
 Gojira                           | Terra Incognita                                 |    38
 Gojira                           | Magma                                           |    44
 Gojira                           | The Link                                        |   185
 Gojira                           | The Way of all Flesh                            |   222
 Gojira                           | L'Enfant Sauvage (Special Edition)              |   270
 Gojira                           | From Mars to Sirius                             |   280
 King Gizzard & The Lizard Wizard | Sketches of Brunswick East                      |    13
 King Gizzard & The Lizard Wizard | I'm In Your Mind Fuzz                           |    15
 King Gizzard & The Lizard Wizard | Chunky Shrapnel                                 |    15
 King Gizzard & The Lizard Wizard | Fishing for Fishies                             |    15
 King Gizzard & The Lizard Wizard | Oddments                                        |    22
 King Gizzard & The Lizard Wizard | Paper Mâché Dream Balloon                       |    28
 King Gizzard & The Lizard Wizard | L.W.                                            |    55
 King Gizzard & The Lizard Wizard | Butterfly 3000                                  |    71
 King Gizzard & The Lizard Wizard | Flying Microtonal Banana                        |    79
 King Gizzard & The Lizard Wizard | Gumboot Soup                                    |    79
 King Gizzard & The Lizard Wizard | Polygondwanaland                                |    92
 King Gizzard & The Lizard Wizard | Murder of the Universe                          |   100
 King Gizzard & The Lizard Wizard | Nonagon Infinity                                |   157
 King Gizzard & The Lizard Wizard | Infest The Rats' Nest                           |   159
 King Gizzard & The Lizard Wizard | K.G.                                            |   161
 Mastodon                         | Cold Dark Place                                 |    11
 Mastodon                         | Medium Rarities                                 |    30
 Mastodon                         | The Hunter                                      |    45
 Mastodon                         | Call of the Mastodon                            |    53
 Mastodon                         | The Hunter (Deluxe)                             |    63
 Mastodon                         | Remission (Reissue)                             |    90
 Mastodon                         | Leviathan                                       |    94
 Mastodon                         | Crack the Skye                                  |   104
 Mastodon                         | Emperor of Sand                                 |   114
 Mastodon                         | Blood Mountain                                  |   135
 Mastodon                         | Once More 'Round the Sun                        |   142
 Melt-Banana                      | Cactuses Come in Flocks                         |    24
 Melt-Banana                      | Scratch or Stitch                               |    38
 Melt-Banana                      | Melt-Banana Lite Live Ver 0.0                   |    57
 Melt-Banana                      | Return of 13 Hedgehogs (Mxbx Singles 2000-2009) |    71
 Melt-Banana                      | Speak Squeak Creak                              |    72
 Melt-Banana                      | Charlie                                         |    81
 Melt-Banana                      | 13 Hedgehogs (Mxbx Singles 1994-1999)           |   102
 Melt-Banana                      | Teeny Shiny                                     |   116
 Melt-Banana                      | Cell-Scape                                      |   142
 Melt-Banana                      | Bambi's Dilemma                                 |   168
 Melt-Banana                      | Fetch                                           |   233
 Protest The Hero                 | Pacific Myth (Deluxe Edition)                   |    23
 Protest The Hero                 | Kezia (Remaster)                                |   115
 Protest The Hero                 | Palimpsest                                      |   170
 Protest The Hero                 | Fortress                                        |   192
 Protest The Hero                 | Scurrilous                                      |   214
 Protest The Hero                 | Volition                                        |   324
 The Black Dahlia Murder          | Unhallowed                                      |    31
 The Black Dahlia Murder          | Verminous                                       |    54
 The Black Dahlia Murder          | Nocturnal                                       |    74
 The Black Dahlia Murder          | Everblack                                       |    77
 The Black Dahlia Murder          | Abysmal                                         |    92
 The Black Dahlia Murder          | Nightbringers                                   |   125
 The Black Dahlia Murder          | Miasma                                          |   141
 The Black Dahlia Murder          | Ritual                                          |   146
 The Black Dahlia Murder          | Deflorate                                       |   247
 The Dillinger Escape Plan        | Irony Is A Dead Scene                           |    24
 The Dillinger Escape Plan        | Calculating Infinity                            |    71
 The Dillinger Escape Plan        | Dissociation                                    |    77
 The Dillinger Escape Plan        | Option Paralysis                                |   136
 The Dillinger Escape Plan        | Miss Machine                                    |   165
 The Dillinger Escape Plan        | Ire Works                                       |   179
 The Dillinger Escape Plan        | One of Us is the Killer                         |   194
(93 rows)
```

---

### 7) For my favorite artists, when did they first enter my listening history?

```sql
WITH favorite_artists(id, track_count) AS (
  SELECT st.artist_id, COUNT(*) AS track_count_by_artist
  FROM scrobbles s
  JOIN spotify_tracks st ON s.spotify_id=st.spotify_id
  GROUP BY st.artist_id
  ORDER BY count(*) desc
  LIMIT 10
)
    SELECT a.name, min(s.timestamp) AS first_listen
    FROM scrobbles s
    JOIN spotify_tracks st ON s.spotify_id=st.spotify_id
    JOIN favorite_artists fa ON st.artist_id=fa.id
    JOIN artist a ON a.spotify_id=st.artist_id
    GROUP BY a.name
    ORDER BY first_listen;
```

```
               name               |      first_listen
----------------------------------+------------------------
 A Wilhelm Scream                 | 2006-05-04 10:25:00-07
 Melt-Banana                      | 2006-05-04 11:44:00-07
 Mastodon                         | 2006-10-27 09:35:00-07
 The Dillinger Escape Plan        | 2008-01-06 09:11:00-08
 Protest The Hero                 | 2009-06-22 15:24:00-07
 Converge                         | 2009-08-29 23:48:00-07
 Between The Buried And Me        | 2009-09-29 03:40:00-07
 The Black Dahlia Murder          | 2009-10-05 20:09:00-07
 Gojira                           | 2012-02-28 08:01:00-08
 King Gizzard & The Lizard Wizard | 2017-12-13 01:07:00-08
(10 rows)
```

---

### 8) What are my favorite artists by first letter of their name?

```sql
CREATE FUNCTION first_letter(text)
  RETURNS text AS 'SELECT SUBSTR($1,1,1)' LANGUAGE sql IMMUTABLE;

SELECT DISTINCT ON (first_letter(a.name)) first_letter(a.name), a.name, COUNT(*) AS listen_count
FROM scrobbles s
JOIN spotify_tracks st ON s.spotify_id=st.spotify_id
JOIN artist a ON st.artist_id=a.spotify_id
GROUP BY first_letter(a.name), a.name
ORDER BY first_letter(a.name), count(*) DESC;
```

```
 first_letter |                     name                     | listen_count
--------------+----------------------------------------------+--------------
 '            | '68                                          |            9
 *            | *NSYNC                                       |            1
 .            | ...And You Will Know Us by the Trail of Dead |           39
 ✝            | ✝✝✝ (Crosses)                                |           11
 $            | $uicideboy$                                  |            2
 1            | 1939 Ensemble                                |            5
 2            | 22                                           |           87
 3            | 311                                          |           44
 4            | 45 Grave                                     |            4
 6            | 65daysofstatic                               |           26
 7            | 7 Seconds                                    |           77
 8            | 8-Bit Misfits                                |            7
 a            | alt-J                                        |          215
 A            | A Wilhelm Scream                             |         1019
 Â            | ÂGE ⱡ TOTAL                                  |            4
 b            | black midi                                   |          195
 B            | Between The Buried And Me                    |         1273
 c            | clipping.                                    |          174
 C            | Converge                                     |         1106
 d            | die Hoffnung                                 |           12
 D            | Deerhoof                                     |          792
 e            | envy                                         |          126
 E            | Exist                                        |          322
 Ē            | Ēriks Ešenvalds                              |            4
 F            | Fallujah                                     |          551
 G            | Gojira                                       |         1075
 h            | headcave                                     |           27
 H            | Haken                                        |          469
 i            | iwrestledabearonce                           |           20
 I            | Intronaut                                    |          524
 J            | Jay Reatard                                  |          460
 K            | King Gizzard & The Lizard Wizard             |         1067
 l            | letlive.                                     |          434
 L            | Leprous                                      |          553
 m            | my bloody valentine                          |          101
 M            | Melt-Banana                                  |         1113
 n            | noose rot                                    |            1
 N            | Nine Inch Nails                              |          454
 o            | of Montreal                                  |            9
 O            | Obscura                                      |          255
 Ö            | Öxxö Xööx                                    |           10
 P            | Protest The Hero                             |         1039
 Q            | Queens of the Stone Age                      |           45
 R            | Refused                                      |          612
 s            | shame                                        |           25
 S            | Skeletonwitch                                |          342
 t            | the bird and the bee                         |           65
 T            | The Black Dahlia Murder                      |          988
 U            | Ulcerate                                     |          110
 V            | Vektor                                       |          432
 w            | will.i.am                                    |            1
 W            | Wrvth                                        |          256
 X            | Xythlia                                      |           30
 Y            | Yautja                                       |           80
 Z            | Zeke                                         |          417
 Б            | БАТЮШКА                                      |            1
 С            | Сруб                                         |            9
(57 rows)
```

#### Comment

I thought these results were pretty interesting. I didn't expect the symbols and cyrillic, and hadn't considered that capitalization would be different, although it's obvious in hindsight. I wrote a version that did LOWER on the first letter, but I decided I liked it better with more results.

---

### 9) What are the longest tracks I’ve listened to more than 10 times?

```sql
SELECT st.track, a.name, st.album, long_tracks.duration, long_tracks.listen_count
FROM spotify_tracks st
JOIN artist a ON a.spotify_id=st.artist_id
JOIN (
  SELECT st.spotify_id, st.duration_ms * '1ms'::interval AS duration, count(*) AS listen_count
  FROM scrobbles s
  JOIN spotify_tracks st ON s.spotify_id=st.spotify_id
  GROUP BY st.spotify_id, st.duration_ms
  HAVING count(*)>10
  ORDER BY st.duration_ms DESC
  LIMIT 10
) AS long_tracks ON long_tracks.spotify_id=st.spotify_id;
```

```
                        track                        |           name            |                album                 |   duration   | listen_count
-----------------------------------------------------+---------------------------+--------------------------------------+--------------+--------------
 Swim To The Moon                                    | Between The Buried And Me | The Great Misdirect                  | 00:17:53.587 |           13
 Tetragrammaton                                      | The Mars Volta            | Amputechture                         | 00:16:41.773 |           12
 The Mighty Masturbator                              | Devin Townsend Project    | Deconstruction (Bonus Track Version) | 00:16:28.253 |           18
 The Architect                                       | Haken                     | Affinity                             | 00:15:40.12  |           16
 Silent Flight Parliament                            | Between The Buried And Me | The Parallax II: Future Sequence     | 00:15:09.307 |           21
 Sovereign                                           | Sunless Dawn              | Timeweaver                           | 00:14:58.827 |           11
 PsUDoPX.046245                                      | PSUDOKU                   | Planetarisk Sudoku                   | 00:14:50.358 |           11
 Same Story                                          | Dionaea                   | Still                                | 00:14:48.764 |           11
 Katana - the Moths and the Dragonflies/Katana - Mud | Manticora                 | To Live to Kill to Live              | 00:14:39.453 |           25
 White Walls                                         | Between The Buried And Me | Colors                               | 00:14:13.213 |           24
(10 rows)
```

---

### 10) What are the 10 artists with longest names in my listening history?

```sql
SELECT a.name
FROM artist a
GROUP BY a.name
ORDER BY length(a.name) DESC
LIMIT 10;
```

```
                             name
---------------------------------------------------------------
 The World Is A Beautiful Place & I Am No Longer Afraid To Die
 The Presidents Of The United States Of America
 ...And You Will Know Us by the Trail of Dead
 El Grupo Nuevo De Omar Rodriguez Lopez
 Cast - The Nightmare Before Christmas
 The (International) Noise Conspiracy
 The Plot to Blow Up the Eiffel Tower
 The Tony Danza Tapdance Extravaganza
 Fredrik Thordendal's Special Defects
 Johnny Thunders & The Heartbreakers
(10 rows)
```

---

### 11) How popular are my favorite artists?

```sql
SELECT a1.name AS artist_name, AVG(st1.popularity) AS track_popularity_average
FROM scrobbles s1
JOIN spotify_tracks st1    ON s1.spotify_id=st1.spotify_id
JOIN artist a1             ON st1.artist_id=a1.spotify_id
JOIN (
  SELECT a.name
  FROM scrobbles s
  JOIN spotify_tracks st    ON s.spotify_id=st.spotify_id
  JOIN artist a             ON st.artist_id=a.spotify_id
  GROUP BY a.name
  ORDER BY count(*) DESC
  LIMIT 10
) AS best_artists ON a1.name=best_artists.name
GROUP BY a1.name
ORDER BY AVG(st1.popularity) DESC;
```

```
           artist_name            | track_popularity_average
----------------------------------+--------------------------
 Gojira                           |      40.1553488372093023
 King Gizzard & The Lizard Wizard |      39.2689784442361762
 Mastodon                         |      36.5084745762711864
 The Black Dahlia Murder          |      31.1680161943319838
 Between The Buried And Me        |      29.6394344069128044
 The Dillinger Escape Plan        |      27.2665890570430733
 Protest The Hero                 |      27.2309913378248316
 Converge                         |      25.7432188065099458
 A Wilhelm Scream                 |      23.4769381746810599
 Melt-Banana                      |      13.0808625336927224
(10 rows)
```

---

### 12) Top 10 most popular artists I’ve listened to more than 20 times?

```sql
SELECT a.name, avg(st.popularity)
FROM scrobbles s 
JOIN spotify_tracks st ON s.spotify_id=st.spotify_id
JOIN artist a ON st.artist_id=a.spotify_id
GROUP BY a.name
HAVING count(*)>20
ORDER BY avg(st.popularity);
```

```
        name        |         avg         
--------------------+---------------------
 Kanye West         | 63.5802469135802469
 Kendrick Lamar     | 61.9064748201438849
 Pearl Jam          | 61.2075471698113208
 The Rolling Stones | 60.9615384615384615
 Tyler, The Creator | 60.0684931506849315
 The Smiths         | 59.3043478260869565
 System Of A Down   | 56.2916666666666667
 Daft Punk          | 55.2500000000000000
 The Beatles        | 55.0201257861635220
 Arctic Monkeys     | 55.0000000000000000
(10 rows)
```

---

### 13) Group listen counts by song length to the nearest minute

```sql
SELECT date_trunc('minute', st.duration_ms * '1ms'::interval + interval '00 second') AS song_length_to_nearest_minute, count(*) AS listen_count
FROM scrobbles s
JOIN spotify_tracks st ON s.spotify_id=st.spotify_id
GROUP BY song_length_to_nearest_minute
ORDER BY song_length_to_nearest_minute;
```

```
 song_length_to_nearest_minute | listen_count
-------------------------------+--------------
 00:00:00                      |         2129
 00:01:00                      |        10943
 00:02:00                      |        19195
 00:03:00                      |        26285
 00:04:00                      |        21058
 00:05:00                      |        12245
 00:06:00                      |         6358
 00:07:00                      |         3466
 00:08:00                      |         1873
 00:09:00                      |         1434
 00:10:00                      |         1058
 00:11:00                      |          456
 00:12:00                      |          416
 00:13:00                      |          163
 00:14:00                      |          140
 00:15:00                      |          118
 00:16:00                      |           69
 00:17:00                      |           34
 00:18:00                      |           26
 00:19:00                      |           34
 00:20:00                      |            9
 00:21:00                      |            7
 00:22:00                      |            8
 00:23:00                      |           12
 00:25:00                      |            2
 00:27:00                      |            3
 00:28:00                      |            6
 00:29:00                      |            1
 00:32:00                      |            2
 00:43:00                      |            7
 00:45:00                      |            2
 01:03:00                      |            2
 01:18:00                      |            1
 01:23:00                      |            1
(34 rows)
```

---

### 14) Top 10 artists with more than 20 listens, having the most average energy?

```sql
SELECT a.name, avg(energetic_tracks.avg_energy)
FROM spotify_tracks st
JOIN artist a ON a.spotify_id=st.artist_id
JOIN (
  SELECT st.artist_id, avg(st.energy) AS avg_energy
  FROM spotify_tracks st
  GROUP BY st.artist_id
  HAVING COUNT(*)>20
) AS energetic_tracks ON energetic_tracks.artist_id=st.artist_id
GROUP BY a.name
ORDER BY avg(energetic_tracks.avg_energy) DESC
LIMIT 10;
```

```
          name           |        avg
-------------------------+--------------------
 White Lung              | 0.9839512153369628
 Shadow of Intent        | 0.9822608724884363
 Zeke                    | 0.9780588265226687
 Allegaeon               | 0.9743333345367798
 Thy Art Is Murder       | 0.9743076929679286
 Outbreak                | 0.9738499939441683
 The Black Dahlia Murder | 0.9725168529856064
 A Wilhelm Scream        | 0.9714905675852067
 Retox                   |  0.970073170778228
 Soilwork                |  0.969612907978796
(10 rows)
```

---

### 15) Top 10 artist I’ve listened to more than 50 times that have the most albums?

```sql
SELECT artist_and_album.name, count(artist_and_album.album)
FROM (
  SELECT DISTINCT a.name, st.album
  FROM spotify_tracks st
  JOIN artist a ON st.artist_id=a.spotify_id
  JOIN (
    SELECT a.spotify_id AS id
    FROM scrobbles s
    JOIN spotify_tracks st ON s.spotify_id=st.spotify_id
    JOIN artist a ON st.artist_id=a.spotify_id
    GROUP BY a.spotify_id
    HAVING count(*) > 50
  ) AS filtered_artists ON a.spotify_id=filtered_artists.id
) AS artist_and_album
GROUP BY artist_and_album.name
ORDER BY count(artist_and_album.album) DESC;
```

```
               name               | count
----------------------------------+-------
 POLYSICS                         |    25
 The Beatles                      |    20
 Nine Inch Nails                  |    20
 Napalm Death                     |    19
 Modest Mouse                     |    18
 Between The Buried And Me        |    18
 Converge                         |    17
 King Gizzard & The Lizard Wizard |    17
 Meshuggah                        |    16
 Deerhoof                         |    16
(10 rows)
```

---

### 16) What month overall do I listen to the most music?

```sql
SELECT TO_CHAR( TO_DATE (EXTRACT(MONTH from timestamp)::text, 'MM'), 'Month') AS month, COUNT(*) AS total_count
FROM scrobbles
GROUP BY month
ORDER BY count(*) DESC
LIMIT 1;
```

```
   month   | total_count
-----------+-------------
 May       |       11677
(1 row)
```

---

### 17) What is my least favorite track on my 10 favorite albums?

```sql
WITH lowest_song_count_by_album(album, song_count) AS (
  SELECT songs.album, min(songs.song_count) AS least_song_count
  FROM (
    SELECT st1.track, st1.album, count(*) AS song_count
    FROM scrobbles s1
    JOIN spotify_tracks st1    ON s1.spotify_id=st1.spotify_id
    JOIN (
      SELECT st.album
      FROM scrobbles s
      JOIN spotify_tracks st    ON s.spotify_id=st.spotify_id
      GROUP BY st.album
      ORDER BY count(*) DESC
      LIMIT 10
    ) AS top_albums ON top_albums.album=st1.album
    GROUP BY st1.track, st1.album
    ORDER BY count(*)
  ) AS songs
  GROUP BY songs.album)
    SELECT st2.track, st2.album, count(*) AS count
    FROM scrobbles s2
    JOIN spotify_tracks st2    ON s2.spotify_id=st2.spotify_id, 
    lowest_song_count_by_album
    WHERE st2.album=lowest_song_count_by_album.album
    GROUP BY st2.track, st2.album
    HAVING count(*)=min(lowest_song_count_by_album.song_count)
    ORDER BY count(*);
```

```
             track             |                    album                     | count 
-------------------------------+----------------------------------------------+-------
 Brain Pain                    | Brain Pain                                   |     1
 Career Suicide                | Career Suicide                               |     2
 No Light Escapes              | All We Love We Leave Behind (Deluxe Edition) |    10
 On My Shield                  | All We Love We Leave Behind (Deluxe Edition) |    10
 Mirage                        | ...Comes to an End                           |    12
 Goodbye to Everything Reprise | The Parallax II: Future Sequence             |    14
 Skies                         | Volition                                     |    16
 Mercy Day For Mr Vengeance    | Ruiner                                       |    16
 Shaking of the Frame          | No Place                                     |    22
 Shuck                         | Shrines                                      |    22
 Ursa Minor                    | Absent Light                                 |    25
 Everything Will Rust          | Absent Light                                 |    25
(12 rows)
```

#### Comment

I think these results demonstrate a bug in last.fm's API. Seemingly, it is less likely to record songs to your listening history when the song title matches the album title. I know both the songs that show up only 1-2 times, and know that I've listened to them more times than are displayed.

---

### 18) What is my favorite track of all time?

```sql
SELECT a1.name, st1.album, st1.track, m.listen_count
FROM scrobbles s1
JOIN spotify_tracks st1   ON s1.spotify_id=st1.spotify_id
JOIN artist a1            ON st1.artist_id=a1.spotify_id
JOIN (
  SELECT s.spotify_id, count(*) AS listen_count
  FROM scrobbles s
  GROUP BY s.spotify_id
  ORDER BY count(*) DESC
  LIMIT 1
) AS m ON m.spotify_id=s1.spotify_id
LIMIT 1;
```

```
   name    |        album        |          track           | listen_count
-----------+---------------------+--------------------------+--------------
 Archspire | Relentless Mutation | Involuntary Doppelgänger |           74
(1 row)
```

---

### 19) Who is my favorite “grunge” band, by listen count?

```sql
SELECT a.name, count(*)
FROM scrobbles s
JOIN spotify_tracks st    ON s.spotify_id=st.spotify_id
JOIN artist a             ON st.artist_id=a.spotify_id
JOIN artist_genre_rel agr ON a.spotify_id=agr.artist_id
JOIN genre g              ON agr.genre_id=g.genre_id
WHERE g.name like '%grunge%'
GROUP BY a.name
ORDER BY count(*) DESC
LIMIT 1;
```

```
  name   | count
---------+-------
 Nirvana |   287
(1 row)
```

---

### 20) Who is my favorite country artist, by listen count? (I don’t listen to country music, but I suspect a little has been recorded over 15 years).

```sql
SELECT a.name, count(*)
FROM scrobbles s
JOIN spotify_tracks st    ON s.spotify_id=st.spotify_id
JOIN artist a             ON st.artist_id=a.spotify_id
JOIN artist_genre_rel agr ON a.spotify_id=agr.artist_id
JOIN genre g              ON agr.genre_id=g.genre_id
WHERE g.name like '%country%'
GROUP BY a.name
ORDER BY count(*) DESC
LIMIT 1;
```

```
           name           | count
--------------------------+-------
 The Reverend Horton Heat |   367
(1 row)
```
