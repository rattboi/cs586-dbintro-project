import csv
import pylast
import tekore as tk
import sqlite3

import settings

network = pylast.LastFMNetwork(api_key=settings.LAST_API_KEY,
                               api_secret=settings.LAST_API_SECRET,
                               username=settings.LAST_USER,
                               password_hash=settings.LAST_PASS_HASH)

app_token = tk.request_client_token(settings.SPOTIFY_CLIENT_ID, settings.SPOTIFY_CLIENT_SECRET)
spotify = tk.Spotify(app_token, sender=tk.RetryingSender(retries=10))

con = sqlite3.connect('project.db')
cur = con.cursor()

cur.execute("""CREATE TABLE IF NOT EXISTS spotify_tracks (
    spotify_id TEXT PRIMARY KEY,
    artist TEXT NOT NULL, 
    album TEXT NOT NULL, 
    track TEXT NOT NULL, 
    popularity INTEGER,
    time_signature INTEGER,
    tempo REAL,
    liveness REAL,
    instrumentalness REAL,
    duration_ms INTEGER,
    energy REAL,
    acousticness REAL
);""")

cur.execute("""CREATE TABLE IF NOT EXISTS scrobbles (
    timestamp DATE NOT NULL,
    spotify_id TEXT NOT NULL,
    PRIMARY KEY (timestamp)
    FOREIGN KEY (spotify_id)
        REFERENCES spotify_tracks (spotify_id)
    );""")

with open('listening_history.csv', newline='') as csvfile:
    scrobble_reader = csv.reader(csvfile, delimiter=',')
    for row in scrobble_reader:
        artist = row[0]
        album = row[1]
        track = row[2]
        try:
            timestamp = row[3]
        except:
            pass
        print(f"{artist} - {track} - {album} -- {timestamp}")

        cur.execute("SELECT timestamp FROM scrobbles WHERE timestamp=?", (timestamp,))
        if len(cur.fetchall()) == 0:
            # Look up artist/album/track via search to get the closest match?
            # or use get_correction somehow on artist/track together
            tracks, = spotify.search(f"{artist} {track} {album}", types=('track',))
            if len(tracks.items) > 0:
                print(f"{tracks.items[0].artists[0].name} {tracks.items[0].album.name} {tracks.items[0].name}")

                spot_artist = tracks.items[0].artists[0].name
                spot_album = tracks.items[0].album.name
                spot_track = tracks.items[0].name
                spotify_id = tracks.items[0].id
                popularity = tracks.items[0].popularity

                try:
                    features = spotify.track_audio_features(spotify_id)
                    time_signature = features.time_signature
                    tempo = features.tempo
                    liveness = features.liveness
                    instrumentalness = features.instrumentalness
                    duration_ms = features.duration_ms
                    energy = features.energy
                    acousticness = features.acousticness
                except:
                    time_signature = 0
                    tempo = 0
                    liveness = 0
                    instrumentalness = 0
                    duration_ms = 0
                    energy = 0
                    acousticness = 0
                
                cur.execute("SELECT timestamp FROM scrobbles WHERE timestamp=?", (timestamp,))
                if len(cur.fetchall()) == 0:
                    print(f"scrobble: {timestamp}")
                    cur.execute("INSERT INTO scrobbles VALUES (?, ?)", (timestamp, spotify_id))

                cur.execute("SELECT spotify_id FROM spotify_tracks WHERE spotify_id=?", (spotify_id,))
                if len(cur.fetchall()) == 0:
                    print(f"track: {spotify_id}: {spot_artist} - {spot_album} - {spot_track}")
                    cur.execute("INSERT INTO spotify_tracks VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (
                        spotify_id, 
                        spot_artist, 
                        spot_album, 
                        spot_track, 
                        popularity,
                        time_signature, 
                        tempo,
                        liveness,
                        instrumentalness,
                        duration_ms,
                        energy,
                        acousticness))

                con.commit()
