import sqlite3
from datetime import datetime

con = sqlite3.connect('project.db')
cur = con.cursor()

count = 0

for row in cur.execute("SELECT spotify_id, name FROM artist").fetchall():
    (spotify_id, artist_name) = row

    print(spotify_id)
    print(artist_name)

    cur.execute("UPDATE spotify_tracks SET artist = ? WHERE artist = ?", (spotify_id, artist_name))
    count = count + 1

print(count)
con.commit()
