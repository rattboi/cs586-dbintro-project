import csv
import pylast
import tekore as tk
import sqlite3

import settings

app_token = tk.request_client_token(settings.SPOTIFY_CLIENT_ID, settings.SPOTIFY_CLIENT_SECRET)
spotify = tk.Spotify(app_token, sender=tk.RetryingSender(retries=10))

con = sqlite3.connect('project.db')
cur = con.cursor()

cur.execute("""CREATE TABLE IF NOT EXISTS artist (
    spotify_id TEXT,
    name TEXT NOT NULL,
    PRIMARY KEY (spotify_id)
);""")

cur.execute("""CREATE TABLE IF NOT EXISTS artist_genre_rel (
    artist_id TEXT NOT NULL,
    genre_id INTEGER NOT NULL,
    PRIMARY KEY (genre_id, artist_id),
    FOREIGN KEY(genre_id) REFERENCES genre,
    FOREIGN KEY(artist_id) REFERENCES artist
);""")

cur.execute("""CREATE TABLE IF NOT EXISTS genre (
    genre_id INTEGER PRIMARY KEY,
    name TEXT
);""")

for row in cur.execute("SELECT spotify_id FROM spotify_tracks").fetchall():
    (track_id,) = row
    a_track = spotify.track(track_id)
    an_artist  = a_track.artists[0]
    print(f"{an_artist.name} - {a_track.album.name} - {a_track.name}")

    cur.execute("SELECT spotify_id FROM artist WHERE spotify_id=?", (an_artist.id,))
    if len(cur.fetchall()) == 0:
        print(f"NEW ARTIST: {an_artist.id}: {an_artist.name}")
        cur.execute("INSERT INTO artist VALUES (?, ?)", (an_artist.id, an_artist.name))

        artist_lookup = spotify.artist(an_artist.id)

        for g in artist_lookup.genres:
            cur.execute("SELECT genre_id FROM genre WHERE name=?", (g,))
            genre_id = cur.fetchone()

            if genre_id is None:
                print(f"NEW GENRE: {g}")
                cur.execute("INSERT INTO genre(name) VALUES (?)", (g,))
                con.commit()

                cur.execute("SELECT genre_id FROM genre WHERE name=?", (g,))
                (genre_id,) = cur.fetchone()
            else:
                (genre_id,) = genre_id

            # now insert into rel table
            cur.execute("INSERT INTO artist_genre_rel VALUES (?,?)", (an_artist.id, genre_id))
            con.commit()
